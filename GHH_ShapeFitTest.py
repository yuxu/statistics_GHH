"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Simple example configuration with input trees and a binned fit            *
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

################################################################
## In principle all you have to setup is defined in this file ##
################################################################

## This configuration extends MyOneBinExample.py to use met/meff shape
## Only two systematics are considered:
##   -JES (Tree-based)
##   -Alpgen Kt scale (weight-based)
##

from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt

import logger
from logger import Logger

from ROOT import gROOT
#gROOT.LoadMacro("./macros/AtlasStyle.C")
import ROOT
#ROOT.SetAtlasStyle()

log = Logger("GHH_ShapeFitTest")
log.setLevel(logger.INFO) #should have no effect if -L is used

useStat = True
#-------------------------------
# Parameters for hypothesis test
#-------------------------------
#configMgr.doHypoTest=False
#configMgr.nTOYs=1000
configMgr.calculatorType=2
configMgr.testStatType=3
configMgr.nPoints=20
configMgr.nTOYs=5000
configMgr.writeXML = True

configMgr.doExclusion=True

#------------------------------------------------------------------------------------------------------
# Possibility to blind the control, validation and signal regions.
# We only have one signal region in this config file, thus only blinding the signal region makes sense.
# the other two commands are only given for information here.
#------------------------------------------------------------------------------------------------------

configMgr.blindSR = True # Blind the SRs (default is False)
configMgr.blindCR = False # Blind the CRs (default is False)
configMgr.blindVR = False # Blind the VRs (default is False)
#configMgr.useSignalInBlindedData = True

#---------------------------------------------------
# Specify the default signal point
# Others to be given via option -g via command line
#---------------------------------------------------

if not "sigSamples" in dir():
    sigSamples=["GHH300Y"]

sysType = "overallNormHisto"

#-------------------------------------
# Now we start to build the data model
#-------------------------------------

# First define HistFactory attributes
configMgr.analysisName = "GHH_ShapeFitTest"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"
configMgr.outputFileName = "results/"+configMgr.analysisName+"_Output.root"
# activate using of background histogram cache file to speed up processes
configMgr.useCacheToTreeFallback = False  # enable the fallback to trees
# enable the use of an alternate data file
configMgr.useHistBackupCacheFile = True
configMgr.histBackupCacheFile =  "data/backupCacheFile_" + configMgr.analysisName + ".root"
#activate using of background histogram cache file to speed up processes
#configMgr.useCacheToTreeFallback = True # enable the fallback to trees
#configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file
#configMgr.histBackupCacheFile =  "data/MyShapeFitExample_template.root" # the data file of your previous fit (= the backup cache file)


# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 138.96516  # Luminosity of input TTree after weighting
configMgr.outputLumi = 138.96516  # Luminosity required for output histograms
configMgr.setLumiUnits("fb-1")



# Set the files to read from
bgdFiles = [configMgr.histCacheFile]

# Dictionnary of cuts for Tree->hist
configMgr.cutsDict["SRSS2lBoostedInc"] = "1."
configMgr.cutsDict["SRSS2lResolvedInc"] = "1."


# Tuples of nominal weights without and with b-jet selection
configMgr.weights = "1."
    
# QCD weights without and with b-jet selection
# we turn the QCD background of for the tutorial as we do not want to use ATLAS data
#configMgr.weightsQCD = "qcdWeight"
#configMgr.weightsQCDWithB = "qcdBWeight"

#--------------------
# List of systematics
#--------------------

# Alpgen KtScale (weight-based)
ktScaleWHighWeights = ("genWeight","eventWeight","ktfacUpWeightW","bTagWeight2Jet")
ktScaleWLowWeights = ("genWeight","eventWeight","ktfacDownWeightW","bTagWeight2Jet")
wzKtScale = Systematic("KtScaleWZ",configMgr.weights,ktScaleWHighWeights,ktScaleWLowWeights,"weight","overallSys")

ktScaleTopHighWeights = ("genWeight","eventWeight","ktfacUpWeightTop","bTagWeight2Jet")
ktScaleTopLowWeights = ("genWeight","eventWeight","ktfacDownWeightTop","bTagWeight2Jet")
#topKtScale = Systematic("KtScaleTop",configMgr.weights,ktScaleTopHighWeights,ktScaleTopLowWeights,"weight","overallSys")
topKtScale = Systematic("KtScaleTop",configMgr.weights,ktScaleTopHighWeights,ktScaleTopLowWeights,"weight","histoSys")
#topKtScale = Systematic("KtScaleTop",configMgr.weights,ktScaleTopHighWeights,ktScaleTopLowWeights,"weight","normHistoSys")

#Systematic(name,nom,high,low,type,method)
#JES (tree-based)
#jes = Systematic("JES","_NoSys","_JESup","_JESdown","tree",sysType + 'Sys')
#configMgr.nomName = "_Nom"

#-------------------------------------------
# List of samples and their plotting colours
#-------------------------------------------
DiBosonSample = Sample("DiBoson", kAzure+1)
DiBosonSample.setNormByTheory()  # scales with lumi
DiBosonSample.setStatConfig(useStat)
#DiBosonSample.setNormFactor("mu_WZ", 1., 0., 500.) #Set this samples's normalization factor by its name and starting, minimum and maximum values.
#DiBosonSample.setNormRegions([("SRSS2lBoostedInc","Meff2l")])

TriBosonSample = Sample("TriBoson",ROOT.kYellow-3)
TriBosonSample.setNormByTheory()
TriBosonSample.setStatConfig(True)
#TriBosonSample.setNormFactor("mu_ssWW",1.,0.,500.)
#TriBosonSample.setNormRegions([("SRSS2lBoostedInc","Meff2l")])

TopXSample = Sample("TopX",ROOT.kGreen-9)
TopXSample.setNormByTheory()
TopXSample.setStatConfig(True)
#TopXSample.setNormFactor("mu_WWW",1.,0.,500.)
#TopXSample.setNormRegions([("SRSS2lBoostedInc","Meff2l")]) #hWWWNom_SRSS2lBoostedIncNorm

OthersSample = Sample("Others",ROOT.kBlue)
OthersSample.setNormByTheory()
OthersSample.setStatConfig(True)

dataSample = Sample("DataNom",kBlack)
dataSample.setData()

## Parameters of the Measurement
measName = "NormalMeasurement"
measLumi = 1.
measLumiError = 0.017 # 2015+16+17+18
#**************
# Exclusion fit
#**************
# loop over all signal points
for sig in sigSamples:
   # Fit config instance
   #exclusionFitConfig = configMgr.addFitConfig("Exclusion_"+sig)
   exclusionFitConfig = configMgr.addFitConfig("Exclusion_"+sig)
   meas=exclusionFitConfig.addMeasurement(measName,measLumi,measLumiError)
   meas.addPOI("mu_SIG")
   
   # Samples
   commonSamples  = [dataSample,OthersSample,TopXSample,DiBosonSample,TriBosonSample] # data and bkg samples 
   exclusionFitConfig.addSamples(commonSamples)
   
   # Signal sample
   sigSample = Sample(sig,ROOT.kViolet)
   sigSample.legName = sig
   sigSample.setNormByTheory()
   sigSample.setStatConfig(True)       
   sigSample.setNormFactor("mu_SIG",1.,0.,50.)
   
   #sigSample.addSampleSpecificWeight("0.001")                
   exclusionFitConfig.addSamples(sigSample)
   exclusionFitConfig.setSignalSample(sigSample)

   # Systematics
   # exclusionFitConfig.getSample("Top").addSystematic(topKtScale)
   # exclusionFitConfig.getSample("WZ").addSystematic(wzKtScale)
   # exclusionFitConfig.addSystematic(jes)
   CRs = []
   SRs = []
   # Channel
   channelSRSS2lBoostedInc = exclusionFitConfig.addChannel("Meff2l",["SRSS2lBoostedInc"],5,400.,2000.)
   channelSRSS2lBoostedInc.useOverflowBin=True
   channelSRSS2lBoostedInc.useUnderflowBin=True
   SRs.append(channelSRSS2lBoostedInc)

   channelSRSS2lResolvedInc = exclusionFitConfig.addChannel("Meff2l",["SRSS2lResolvedInc"],5,200.,1000.)
   channelSRSS2lResolvedInc.useOverflowBin=True
   channelSRSS2lResolvedInc.useUnderflowBin=True
   SRs.append(channelSRSS2lResolvedInc)

   exclusionFitConfig.addSignalChannels(SRs)
   
